To build:
$ atlas-package

To use:
* Install into Confluence using the UPM
* Navigate to 'Log Downloader' in the administration console
* click the log file you wish to download
NOTE: You must have administrative permissions to use the log downloader

Supports:
Confluence 5.1.5
