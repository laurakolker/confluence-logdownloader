package net.artemissoftware.confluence.plugins.tools.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.artemissoftware.confluence.plugins.tools.actions.ViewLogDownloader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;

/**
 */
public class LogDownloaderServlet extends HttpServlet {


    private static final String PARAM_FILENAME = "filename";
    private static final String SAVETOSERVER_DIR = "logs";
    private final BootstrapManager bootstrapManager;
    private final PermissionManager permissionManager;

    protected static final Logger log = LoggerFactory.getLogger(LogDownloaderServlet.class);

    public LogDownloaderServlet(BootstrapManager bootstrapManager, PermissionManager permissionManager) {
        this.bootstrapManager = bootstrapManager;
        this.permissionManager = permissionManager;
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //check permissions
        User user = AuthenticatedUserThreadLocal.getUser();
        if (!this.permissionManager.hasPermission(user, Permission.ADMINISTER, PermissionManager.TARGET_APPLICATION)) {
            String name = (user == null) ? "null" : user.getName();
            log.error("Log Downloader - User does not have permission to download file: " + name);
            resp.sendRedirect(req.getContextPath() + "/admin/artemissoftware/logdownloader/viewlogdownloader.action");
        }

        //identify the requested file
        String filename = req.getParameter(PARAM_FILENAME);
        File file = getFile(filename);

        if (file == null) {
            log.error("Log Downloader - Could not find file: " + filename);
        } else {
            //download the file
            writeFileToResponse(req, resp, filename, file);
            log.info("Log Downloader - Downloaded file: " + filename);
        }
    }

    protected void writeFileToResponse(HttpServletRequest req, HttpServletResponse resp,
                                       String filename, File file) throws IOException {
        if (!filename.endsWith(".log")) {
            return;
        }
        //set some headers
        resp.setHeader("Content-disposition", "attachment; filename=" + filename);
        //XXX Do we want to try to get the mime type?
        //		ServletContext context  = getServletConfig().getServletContext();
        //		String mimetype = context.getMimeType(filename);
        //		if (mimetype != null) resp.setContentType(mimetype);

        //write
        OutputStream out = resp.getOutputStream();
        FileInputStream in = new FileInputStream(file);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.flush();
        out.close();
    }

    /* File IO */
    protected File getFile(String filename) {
        String home = bootstrapManager.getConfluenceHome(); //5.1.5 version of the method. This is deprecated later
        File homedir = new File(home);
        if (homedir.exists()) {
            String logpath = home + File.separator + ViewLogDownloader.LOG_DIR + File.separator + filename;
            File log = new File(logpath);
            if (log.exists()) return log;
        }
        return null;
    }


}