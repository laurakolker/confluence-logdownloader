package net.artemissoftware.confluence.plugins.tools.actions;

import java.io.File;
import java.util.List;
import java.util.Vector;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;

public class ViewLogDownloader extends ConfluenceActionSupport {

	private List<String> filenames;
	public static final String LOG_DIR = "logs";
	private final BootstrapManager bootstrapManager;
	private final PermissionManager permissionManager;
	public ViewLogDownloader(BootstrapManager bootstrapManager,
			PermissionManager permissionManager) {
		this.bootstrapManager = bootstrapManager;
		this.permissionManager = permissionManager;
	}
	
	public String execute() {// WebSudo
		User user = AuthenticatedUserThreadLocal.getUser();
		if (!this.permissionManager.hasPermission(user, Permission.ADMINISTER, PermissionManager.TARGET_APPLICATION)) {
			getActionErrors().add("You do not have permission to view the log files.");
		}
		else {
            findLogFiles();
		}
		return SUCCESS;
	}

	private void findLogFiles() {
		List<String> filenames = getFilenames();
		String home = bootstrapManager.getConfluenceHome(); //5.1.5 version of the method. This is deprecated later
		File homedir = new File(home);
		if (homedir.exists()) {
			String logpath = home + File.separator + LOG_DIR;
			File logdir = new File(logpath);
			if (!logdir.exists()) return;
			
			File[] listFiles = logdir.listFiles();
			for (File file : listFiles) {
				if (file.isFile()) filenames.add(file.getName());
			}
		}
	}

	public List<String> getFilenames() {
		if (filenames == null)
			filenames = new Vector<String>(); 
		return filenames;
	}

	public void setFilenames(List<String> filenames) {
		this.filenames = filenames;
	}
	
}
