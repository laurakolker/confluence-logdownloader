AJS.toInit(function() {
	$('.artemis-logdownloader-filelink').click(function(e) {
		var name = $(this).text();
		$('#artemis-logdownloader-filename').val(name);
		document.forms.artemislogdownloaderform.submit();
		e.preventDefault();	
	});
});
